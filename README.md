# README #

This is a small project where I implemented Sebastien Lagarde's volume interpolation algorithm with javascript. Since the pseudo code from Lagarde's blog had some singularities and things I didn't quite understand, I modified it a bit. The algorithm is currently capable of blending arbitrary box volumes seamlessly. The rendering is a simple point based one, with the help of a javascript canvas.

![interpolation_js_normalized.PNG](https://bitbucket.org/repo/RaxR7r/images/1500341687-interpolation_js_normalized.PNG)